﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Labs5Kurs.Models
{
    public class Klaster
    {
        public List<Sample> Samples { get; set; }//данные
        public int Dimention { get; set; }//колличество выборок
        public string ClassException { get; set; }//ошибки

        public Klaster(string DataString)
        {
            if (Samples != null)
                Samples.Clear();

            ClassException = "";
            var MyData= GetKlaster(DataString);
            Samples = new List<Sample>();
            foreach (var sample in MyData)
            {
                Samples.Add(new Sample(sample));
            }
            
        }
        public List<List<double>> GetKlaster(string FileText)
        {
            try
            {
                List<List<double>> result = new List<List<double>>();
                var kek = 0;
                var dot = FileText.Split("\r\n");
                Dimention = dot[0].Split(' ').Length;
                for (int i = 0; i < Dimention; i++)
                    result.Add(new List<double>());

                for (int i = 0; i < dot.Length; i++)
                {
                    var level = dot[i].Split(' ');
                    for (int j = 0; j < level.Length; j++)
                    {
                        result[j].Add(Convert.ToDouble(level[j]));
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                ClassException += ex.Message + "\n";
                return new List<List<double>>();
            }
        }
    }
}
