﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.IO;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;


namespace Labs5Kurs.Models
{
    public class FileModel
    {
        public string FileName { get; set; }
        public string? FileText { get; set; }
        public Klaster Klaster { get; set; }


        public FileModel( string filename, string filetext)
        {
            FileName = filename;
            FileText = filetext;
            Klaster = new Klaster(filetext);


        }
        
    }

}
