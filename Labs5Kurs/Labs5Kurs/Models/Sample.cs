﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;

namespace Labs5Kurs.Models
{
    public class Sample
    {
        public List<double> Data { get; set; }//данные
        public double[] Ni { get; set; }//Список Гистограм
        public string[] NiText { get; set; }//Список Гистограм
        public int NAmount { get; set; }//Коллисечтво классов в каждой выборке
        public double H { get; set; }//шаг разбития в каждой выборке
        public double[] Axis { get; set; }
        public List<MyPoint> Smooth { get; set; }
        public double XMin { get; set; }
        public double XMax { get; set; }

        public Sample(List<double> data)
        {
            try
            {
                Data = data;
                XMin = Data.Min();
                XMax = Data.Max();
                GetGistortams();
            }
            catch (Exception)
            {
                throw;
            }
        }
        public void GetGistortams()
        {
            
            NAmount = Convert.ToInt32(Math.Pow(Data.Count, Data.Count > 100 ? 1.0 / 3.0: 1.0 / 2.0));
            if (NAmount < 3)
                NAmount = 3;
            //if (Data.Count > 100)
            //    NAmount = Convert.ToInt32(Math.Pow(Data.Count, 1.0 / 3.0));
            //else
            //    NAmount = Convert.ToInt32(Math.Sqrt(Data.Count));
            H = (Data.Max() - Data.Min()) / (double)NAmount;

            Ni = new double[NAmount + 1];
            for (int j = 0; j < Data.Count; j++)
                Ni[Convert.ToInt32(Math.Truncate((Data[j] - Data.Min()) / H))]++;
            for (int i = 0; i < Ni.Length; i++)
                Ni[i] /= Data.Count;

            NiText = new string[NAmount+1];
            for (int j = 0; j < NiText.Length; j++)
                NiText[j] = (Convert.ToString(Math.Round(Data.Min() + H * j, 2)) + " - " + Convert.ToString(Math.Round(Data.Min() + H * (j + 1), 2)));

            Axis=new double[NAmount + 1];
            for (int i = 0; i < Axis.Length; i++)
                Axis[i] = Math.Round(Data.Min() + H * ((double)i + 1) - (double)H / (double)2, 4);
            var NSmooth = 10;
            var Krat = Ni.Length;
            Smooth = new List<MyPoint>();
            //for (int i = 0; i < Krat; i++)
            //{
            //    double pm1 = Ni[i * 3];
            //    double p = Ni[(i * 3) + 1];
            //    double pp1 = Ni[(i * 3) + 2];
            //    var xStart = Data.Min() + H * i * 3;
            //    var xEnd = Data.Min() + H * i * 3 + H*3;
            //    var Path = xEnd - xStart;
            //    var hPath = Path / NSmooth;
            //    for (int j = 0; j < NSmooth; j++)
            //    {
            //        var x = xStart + hPath * j;
            //        Smooth.Add(new MyPoint(x, SmoothF(x, pm1, p, pp1)));
            //    }
            //}
            for (int i = 0; i < Krat; i++)
            {
                
                double pm1 = (i==0)?0: Ni[i-1];
                
                double p = Ni[i];
                double pp1 = (i == Krat-1) ?0: Ni[i+1];
                var xStart = Data.Min() + H * i;
                var xEnd = Data.Min() + H * (i+1);
                var Path = xEnd - xStart;
                var hPath = Path / NSmooth;
                for (int j = 0; j < NSmooth; j++)
                {
                    var x = xStart + hPath * j;
                    Smooth.Add(new MyPoint(x, SmoothF(x, pm1, p, pp1)));
                }
            }


        }
        double SmoothF(double x,double pm1,double p,double pp1)
        {
            double s1=(1d/8d)*(pm1+6d*p+pp1);
            double s2=(1d/4d)*(pp1 - pm1)*x;
            double s3=(1d / 8d) * (pm1 - 2d * p + pp1) * x * x;
            return s1 + s2 + s3;
        }

    }
    public class MyPoint
    {
        public double x { get; set; }
        public double y { get; set; }
        public MyPoint(double x,double y)
        {
            this.x = x;
            this.y = y;
        }
    }
}
