﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;
using Labs5Kurs.ViewModels;
using Microsoft.AspNetCore.Identity;

using System;
using System.Collections.Generic;
using System.Linq;

namespace Labs5Kurs.Controllers
{
    public class AccountController : Controller
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly SignInManager<IdentityUser> _signInManager;

        public AccountController(UserManager<IdentityUser> userManager, SignInManager<IdentityUser> signInManager)
        {
            _userManager = userManager;
            _signInManager = signInManager;
        }
        public IActionResult Login()
        {
            return View();
        }
        [HttpPost]
        public IActionResult Login([FromBody] LoginViewModel loginViewModel)
        {
            if (ModelState.IsValid)
            {
                var result = _signInManager.PasswordSignInAsync(loginViewModel.Email, loginViewModel.Password, loginViewModel.RememberMe, false);
                result.Wait();
                if (result.Result.Succeeded)
                {
                    return Ok();
                }
                else
                {
                    return Forbid();
                }
            }
            return BadRequest();
        }

        [HttpPost]
        public IActionResult Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                IdentityUser user = new IdentityUser { Email = model.Email, UserName = model.Email };
                // добавляем пользователя
                var result = _userManager.CreateAsync(user, model.Password);
                result.Wait();

                if (result.Result.Succeeded)
                {
                    return Ok();
                }
                else
                {
                    return StatusCode(500);
                }
            }
            return BadRequest();
        }
        [HttpPost]
        public async Task<IActionResult> Logout()
        {
            // удаляем аутентификационные куки
            await _signInManager.SignOutAsync();
            return RedirectToAction("Login", "Account");
        }


    }
}
