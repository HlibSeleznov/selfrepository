﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;
using Labs5Kurs.Models;
using Microsoft.AspNetCore.Authorization;
using Labs5Kurs.ViewModels;
using System.IO;

namespace Labs5Kurs.Models
{
    [Authorize]
    public class HomeController:Controller
    {
        private readonly ILogger<HomeController> _logger;
        private ApplicationContext _dbContext;
        //private IHostingEnvironment Environment;

        public HomeController(ILogger<HomeController> logger, ApplicationContext dbContext/*, IHostingEnvironment _environment*/)
        {
            _logger = logger;
            _dbContext = dbContext;
        }
        public IActionResult Index()
        {
            var files = GetFiles();
            return View(files);
            ///_dbContext.Settings.ToList();
            //return View();
        }
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
        public List<FileModel> GetFiles()
        {
            var kekDirectory = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location).Split("\\");

            var kekNameProject1 = System.Reflection.Assembly.GetExecutingAssembly().FullName.Split(',')[0];
            var k2 = 0;

            string kekwwwroot = "";
            for (int i = 0; i < kekDirectory.Length; i++)
            {
                if (kekDirectory[i] != kekNameProject1)
                    kekwwwroot += kekDirectory[i] + "\\";
                else
                {
                    kekwwwroot += kekDirectory[i] + "\\";
                    break;
                }
            }
            kekwwwroot += kekNameProject1 + "\\" + "wwwroot\\Files";
            var k = 0;

            string[] filePaths = Directory.GetFiles(kekwwwroot);

            //Copy File names to Model collection.
            List<FileModel> files = new List<FileModel>();
            foreach (string filePath in filePaths)
            {
                var Name = Path.GetFileName(filePath);
                var Text = System.IO.File.ReadAllText(filePath);
                FileModel File = new FileModel(Name, Text);
                files.Add(File);
            }
            return files;

        }

    }
}
