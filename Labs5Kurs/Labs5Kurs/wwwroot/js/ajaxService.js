﻿var AJAX_URL = {
    ACCOUNT_LOGIN: "../../Account/Login",
    ACCOUNT_LOGOUT: "/Account/Logout",
    HOME_INDEX: "../../Home/Index",
    HOME: "../../Home/Main",
    API_EVENT: "/api/Event",
    API_BILLING: "/api/Billing",
    ACCOUNT_CHANGEPASS: "../../Account/ChangePassword",
    SETTINGS_UPDATE: "../../Settings/ChangeMessagesSettings",
    API_PARENT_EDIT_CHILD: "../../api/Parent/EditChild",
    API_PARENT_CHANGE_FIO: "../../api/Parent/ChangeFIO",
    API_PARENT_CHANGE_PHOTO: "../../api/Parent/ChangePhoto",
    API_ADMIN_CREATE_CHILD: "../../api/Admin/CreateChild",
    API_ADMIN_CREATE_SCHOOL: "../../api/Admin/CreateSchool",
    API_ADMIN_DELETE_SCHOOL: "../../api/Admin/DeleteSchool",
    API_ADMIN_UPDATE_SCHOOL: "../../api/Admin/UpdateSchool",
    API_ADMIN_CREATE_PARENT: "../../api/Admin/CreateParent",
    API_PARENT_CHANGE_CHILD_PHOTO: "../../api/Parent/ChangeChildPhoto",
    ACCOUNT_REGISTER: "../../Account/RegisterNewUser"
};

function showHideLoader() {
    var loader = $("#loader");
    if (loader.is(":visible")) {
        loader.fadeOut();
    }
    else {
        loader.fadeIn();
    }
}

function getCulture() {
    try {
        var cultureString = Cookies.get(".AspNetCore.Culture").split("|");
        return cultureString[0].replace("c=", "")
    }
    catch {
        return "uk";
    }
}

function showHideUploadLoader() {
    var loader = $("#fileLoader");
    if (loader.is(":visible")) {
        loader.fadeOut();
    }
    else {
        loader.fadeIn();
    }
}

function changeUploadStatus(percent) {
    $("#uploadPercent").text(parseInt(percent));
}

function sendJsonAjaxRequest(actionLink, paramsArray, onCompleteFunction, on400Head, on400Message, elementName, isAsync) {
    return $.ajax({
        async: isAsync,
        cache: false,
        type: "POST",
        url: actionLink,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify(paramsArray),
        beforeSend: function () {
            showHideLoader();
        },
        complete: function (data) {
            if (typeof onCompleteFunction === "function") {
                showHideLoader();
                if (data.status == 200) {
                    onCompleteFunction(data);
                }
                else {
                    errorByData(data, on400Head, on400Message, elementName);
                }
            }
        }
    });
}

function sendFormAjaxRequest(actionLink, form, onCompleteFunction, on400Head, on400Message, elementName, isAsync){
    return $.ajax({
        async: isAsync,
        cache: false,
        type: "POST",
        url: actionLink,
        data: form.serialize(),
        complete: function (data) {
            if (typeof onCompleteFunction === "function") {
                showHideLoader();
                if (data.status == 200) {
                    onCompleteFunction(data);
                }
                else {
                    errorByData(data, on400Head, on400Message, elementName);
                }
            }
        }
    });
}

function thankYou() {
    var title;
    switch (getCulture()) {
        case "uk": {
            title = "Дякуємо";
            break;
        }
        case "ru": {
            title = "Спасибо";
            break;
        }
        default: {
            title = "Thank you";
            break;
        }
    }

    Swal.fire({
        icon: 'success',
        title: title
    })
}

function warningModal(head, text) {
    Swal.fire({
        icon: 'warning',
        title: head,
        text: text,
    });
}

function errorByData(data, on400Head, on400Message, elementName) {
    switch (data.status) {
        case 400: {
            Swal.fire({
                icon: 'warning',
                title: on400Head,
                html: on400Message,
            })
            break;
        }
        case 500: {
            var title, text;
            switch (getCulture()) {
                case "uk": {
                    title = "Упсс!";
                    text = "Щось пішло не так...";
                    break;
                }
                case "ru": {
                    title = "Упсс!";
                    text = "Что-то пошло не так...";
                    break;
                }
                default: {
                    title = "Oops!";
                    text = "Something went wrong...";
                    break;
                }
            }

            Swal.fire({
                icon: 'error',
                title: title,
                text: text
            })
            break;
        }
        case 403: {
            var title, text;
            console.log(getCulture());
            console.log("fgd");
            switch (getCulture()) {
                case "uk": {
                    title = "Вибачте!";
                    text = "Доступ заборонено";
                    break;
                }
                case "ru": {
                    title = "Извините!";
                    text = "Доступ запрещен";
                    break;
                }
                default: {
                    title = "Sorry!";
                    text = "Access denied";
                    break;
                }
            }

            Swal.fire({
                icon: 'error',
                title: title,
                text: text,
            })
            break;
        }
        case 404: {
            var title, text;

            switch (getCulture()) {
                case "uk": {
                    title = "Упсс!";
                    text = "Елементів не знайдено...";
                    break;
                }
                case "ru": {
                    title = "Упсс!";
                    text = "Элементов не найдено...";
                    break;
                }
                default: {
                    title = "Oops!";
                    text = "Elements does not exists...";
                    break;
                }
            }

            Swal.fire({
                icon: 'error',
                title: title,
                text: text,
            })
            break;
        }
        case 409: {
            var title, text;
            switch (getCulture()) {
                case "uk": {
                    title = "Увага!";
                    text = `Такий ${elementName} вже існує`;
                    break;
                }
                case "ru": {
                    title = "Внимание!";
                    text = `Такой ${elementName} уже существует`;
                    break;
                }
                default: {
                    title = "Warning!";
                    text = `Element ${elementName} is already exists`;
                    break;
                }
            }

            Swal.fire({
                icon: 'warning',
                title: title,
                text: text,
            })
            break;
        }
    }
}

function sendAjaxRequest(actionLink, paramsArray, onSuccessFunction, onErrorFunction, onFailureFunction) {
    return $.ajax({
        async: true,
        cache: false,
        type: "POST",
        url: actionLink,
        data: paramsArray,
        beforeSend: function () {
            showHideLoader();
        },
        success: function (data) {
            if (typeof onSuccessFunction === "function") {
                showHideLoader();
                onSuccessFunction(data);
            }
        },
        failure: function (response) {
            if (typeof onSuccessFunction === "function") {
                showHideLoader();
                onFailureFunction(response);
            } else {
                showHideLoader();
                console.log(response);
            }
        },
        error: function (response) {
            if (typeof onErrorFunction === "function") {
                showHideLoader();
                onErrorFunction(response);
            } else {
                showHideLoader();
                console.log(response);
            }
        }
    });
}

function sendAjaxFormData(actionLink, formData, onSuccessFunction, onFailureFunction) {
    var xhr = new XMLHttpRequest();
    showHideUploadLoader();
    xhr.upload.onprogress = function (event) {
        var percent = event.loaded / event.total * 100;
        changeUploadStatus(percent);
    };

    xhr.onloadend = function () {
        showHideUploadLoader();
        if (xhr.status == 200) {
            onSuccessFunction(xhr);
        } else {
            onFailureFunction(xhr);
        }
    };

    xhr.open("POST", actionLink);
    xhr.send(formData);
}

function uploadFile(files, onSuccess) {
    var formData = new FormData();
    if (files != null) {
        for (var i = 0; i < files.length; i++) {
            formData.append('file', files[i]);
        }
        var _url = '../../api/File/UploadSimpleFile';
        sendAjaxFormData(_url, formData, onSuccess, function (data) {
            console.log(data);
            var title, text;
            switch (getCulture()) {
                case "uk": {
                    title = "Упсс!";
                    text = "Виникла помилка...";
                    break;
                }
                case "ru": {
                    title = "Упсс!";
                    text = "Произошла ошибка";
                    break;
                }
                default : {
                    title = "Oops!";
                    text = "Some error has occured...";
                    break;
                }
            }
            warningModal(title, text);
        });
    }
}

const TaskStatus = {
    Created: "0",
    WaitingForActivation: "1",
    WaitingToRun: "2",
    Running: "3",
    WaitingForChildrenToComplete: "4",
    RanToCompletion: "5",
    Canceled: "6",
    Faulted: "7"
}